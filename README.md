[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&prefix=v&query=version&url=https%3A%2F%2Fgitlab.com%2Ffoundryvtt-pt-br%2Fcore%2F-%2Fraw%2Fmaster%2Fpt-BR%2Fmodule.json)](https://gitlab.com/foundryvtt-pt-br/core) [![Min Core](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Min%20Core&prefix=v&query=minimumCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundryvtt-pt-br%2Fcore%2F-%2Fraw%2Fmaster%2Fpt-BR%2Fmodule.json)](http://foundryvtt.com/) [![Compatible](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Compatible&prefix=v&query=compatibleCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundryvtt-pt-br%2Fcore%2F-%2Fraw%2Fmaster%2Fpt-BR%2Fmodule.json)](http://foundryvtt.com/) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow)](https://opensource.org/licenses/MIT) [![Discord invite](https://img.shields.io/badge/Chat-on_Discord-blue?logo=discord&logoColor=white)](https://discordapp.com/invite/DDBZUDf) [![Translated](https://img.shields.io/endpoint?url=https://l10n.creativelabs.dev/fvtt-core)](https://www.transifex.com/foundryvtt-brasil/fvtt-core/)



Portuguese (BR) Core
=================================

## Português

Esse módulo adiciona a opção de selecionar o idioma Português (Brasil) no menu de configurações do [FoundryVTT](http://foundryvtt.com/ "Foundry Virtual Tabletop"). Ao selecionar essa opção você obterá a tradução de diversos aspectos da interface do programa.

### Instalação

Na opção `Add-On Modules` clique em `Install Module` e coloque o seguinte link no campo `Manifest URL`

`https://gitlab.com/foundryvtt-pt-br/core/-/raw/master/pt-BR/module.json`

Se essa opção não funcionar faça o download do arquivo [pt-BR.zip](https://gitlab.com/foundryvtt-pt-br/core/-/jobs/artifacts/master/raw/pt-BR.zip?job=build "pt-BR.zip") e extraia o conteúdo dele dentro da pasta `Data/modules/`

Feito isso ative o módulo nas configurações do mundo em que pretende usá-lo e depois altere o idioma nas configurações.

### Contribuindo

A forma principal de contribuição para o projeto é através da tradução dos termos em inglês disponiveis na plataforma Zanata, que podem ser encontrados nesse [link](https://translate.zanata.org/project/view/fvtt-core "Zanata - FVTT Core"). Para mais informações leia o [guia de contribuição](CONTRIBUTING.md "Guia de contribuição")


---




## English

This module adds the option to select the *Português (Brasil)* language from the [FoundryVTT](http://foundryvtt.com/ "Foundry Virtual Tabletop") settings menu. Selecting this option will translate various aspects of the program interface.


### Installation

In the `Add-On Modules` option click on `Install Module` and place the following link in the field `Manifest URL`

`https://gitlab.com/foundryvtt-pt-br/core/raw/master/pt-BR/module.json`

If this option does not work download the [pt-BR.zip](https://gitlab.com/foundryvtt-pt-br/core/-/jobs/artifacts/master/raw/pt-BR.zip?job=build "pt-BR.zip") file and extract its contents into the `Data/modules/` folder

Once this is done, enable the module in the settings of the world in which you intend to use it and then change the language in the settings.

### Contributing

The main form of contribution to the project is through the translation of the English terms available on the Zanata platform, which can be found at this [link](https://translate.zanata.org/project/view/fvtt-core "Zanata - FVTT Core"). For more information, read the [contribution guide](CONTRIBUTING.md "Contribution guide")
